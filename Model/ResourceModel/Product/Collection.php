<?php

namespace Gateway3D\AutoImport\Model\ResourceModel\Product;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	/**
	 * Define model & resource model
	 */
	protected function _construct()
	{
		$this->_init(
			'Gateway3D\AutoImport\Model\Product',
			'Gateway3D\AutoImport\Model\ResourceModel\Product'
		);
	}
}