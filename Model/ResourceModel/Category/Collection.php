<?php

namespace Gateway3D\AutoImport\Model\ResourceModel\Category;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	/**
	 * Define model & resource model
	 */
	protected function _construct()
	{
		$this->_init(
			'Gateway3D\AutoImport\Model\Category',
			'Gateway3D\AutoImport\Model\ResourceModel\Category'
		);
	}
}