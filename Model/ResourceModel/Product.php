<?php

namespace Gateway3D\AutoImport\Model\ResourceModel;

class Product  extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}

	/**
	 * Define main table
	 */
	protected function _construct()
	{
		$this->_init('gateway3d_autoimport_datasource_abstract_product', 'id');
	}
}