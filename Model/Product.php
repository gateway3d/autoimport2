<?php

namespace Gateway3D\AutoImport\Model;

use Gateway3D\AutoImport\Helper\Data;
use Magento\Framework\Model\AbstractModel;

class Product extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'gateway3d_autoimport_product';
	protected $_cacheTag = 'gateway3d_autoimport_product';
	protected $_eventPrefix = 'gateway3d_autoimport_product';

	protected $serialisedData = null;
	protected $logger;
	protected $_helper;

	protected $ATTR_MAP = array(
		//General attributes
		"description"=>'short_description',
		'long_description'=>'description',
		'weight' => 'weight',

		//Personaliseit attributes
		'personaliseit_m_iframe_url' => 'g3d_app_url_mobile',
		'personaliseit_iframe_url' => 'g3d_app_url_default',
		'image_external_url' => 'g3d_external_image',

		//Gateway3D Autoimport attributes
		'supplier'=>'g3d_supplier',
		'type'=>'g3d_type',
		'lead_time'=>'g3d_lead_time',
		'price_includes'=>'g3d_price_includes',
		'manufacturers_code'=>'g3d_manufacturers_code',
		'selling_unit'=>'g3d_selling_unit',
		'third_party_personalisation_URL'=>'g3d_third_party_url',
		'attribute_sets' => 'attribute_sets'

	);

	protected $ATTR_MAP_ECOMMERCE = array(
		'licence_percentage' => 'g3d_licence_percentage',
		'dimensions' => 'g3d_dimensions',
		'nominal_code' => 'g3d_nominal_code',
		'packaging_sku' => 'g3d_packaging_sku',
		'barcode'=>'g3d_barcode',
		'notes' => 'g3d_notes',
		'website_category_details' => 'g3d_website_category_details',
		'meta_description' => 'meta_description',
		'meta_title' => 'meta_title',
		'brand_code' => 'g3d_brand_code',
		'design_name' => 'g3d_design_name',
		'licence' => 'g3d_licence',
		'lead_time_days' => 'g3d_lead_time_days',
		'standard_margin' => 'g3d_standard_margin',
		'tax_code' => 'g3d_tax_code',
		'increment_quantity' => 'g3d_increment_quantity',
		'certificate_url' => 'g3d_certificate_url',
		'production_instructions' => 'g3d_production_instructions',
		'brand_name' => 'g3d_brand_name',
		'brand_name_short' => 'g3d_brand_name_short',
		'hs_code' => 'g3d_hs_code'
	);

	protected $ATTR_MAP_TIERS = array(
		'rrp' => 'g3d_rrp'
	);

	protected $_optionData = array();
	protected $_groups = array();

	protected $_moduleManager;


	/**
	 * Define resource model
	 */
	protected function _construct()
	{
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/autoimport.log');
		$myLogger = new \Zend\Log\Logger();
		$myLogger->addWriter($writer);
		$this->logger = $myLogger;
		$this->_init('Gateway3D\AutoImport\Model\ResourceModel\Product');
	}


	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];
		return $values;
	}

	private function checkUrlKeyDuplicates($storeRepository, $sku, $urlKey) 
	{	
		$originalUrlKey = $urlKey;
		$urlKey .= '.html';

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$conn = $objectManager->create('\Magento\Framework\App\ResourceConnection');
		$connection = $conn->getConnection();

		$tablename = $connection->getTableName('url_rewrite');

		$unique = true;

		$stores = $storeRepository->getList();
		foreach ($stores as $store) {
			$sql = $connection->select()->from(
							['url_rewrite' => $connection->getTableName('url_rewrite')], ['request_path', 'store_id']
					)->joinLeft(
							['cpe' => $connection->getTableName('catalog_product_entity')], "cpe.entity_id = url_rewrite.entity_id"
					)->where('request_path IN (?)', $urlKey)
					->where('store_id IN (?)', $store['website_id'])
					->where('cpe.sku not in (?)', $sku);
			$urlKeyDuplicates = $connection->fetchAssoc($sql);

			if (!empty($urlKeyDuplicates)) {
				$unique = false;
			}

			$sql2 = $connection->select()->from(
				['catalog_product_entity_varchar' => $connection->getTableName('catalog_product_entity_varchar')], ['value', 'store_id']
			)
			->where('value like (?)', $originalUrlKey)
			->where('store_id IN (?)', $store['website_id']);
			$urlKeyDuplicates = $connection->fetchAssoc($sql2);

			if (!empty($urlKeyDuplicates)) {
				$unique = false;
			}
		}
		return $unique;
	}

	public function createUrlKey($storeRepository, $title, $sku) 
	{
		$url = preg_replace('#[^0-9a-z]+#i', '-', $title);
		$urlKey = strtolower($url);
		
		$isUnique = $this->checkUrlKeyDuplicates($storeRepository, $sku, $urlKey);
		if ($isUnique) {
			return $urlKey;
		} else {
			return $urlKey . '-' . time() . rand(1,9);
		}
	}

	public function updateMagento($product, $helper, $categoryFactory, $productFactory, $storeRepository, $productResourceModel, $moduleManager)
	{
		$this->_helper = $helper;
		
		$mSku = $this->loadSerialisedData("sku");
		$mRef = $this->loadSerialisedData("ref");
		$discontinued = $this->loadSerialisedData("is_discontinued");
		$retailSku = $this->loadSerialisedData("retail_sku");
		$packagingSku = '';
		$importSkuFrom = $helper->getSettings('import_sku_from');
		if ($importSkuFrom == 'retail_sku') $mSku = $retailSku;
		$groupingByPrintSizes = $helper->getSettings('enable_grouping_by_print_sizes');
		$externalImage = '';
		$external3dImage = $this->loadSerialisedData("3d_image_url");

		//import to website
		$importToWebsite = $helper->getSettings('import_to_website');
		$importToWebsiteArray = explode(",",$importToWebsite);
		$overwriteWebsite = $helper->getSettings('overwrite_website');
		$overwriteMetaTitle = $helper->getSettings('overwrite_product_meta_title');
		$overwriteMetaDescription = $helper->getSettings('overwrite_product_meta_description');
		$metaTitle = '';

		//weight unit
		$weightDivider = ($helper->getWeightUnit() == 'lbs') ? 454 : 1000;

		//add datasource code to sku
		$addDatasourceCodeToSku = $helper->getSettings('add_datasource_code_to_sku') ? true: false;
		if ($addDatasourceCodeToSku)
		{
			if($mSku && $mRef)
			{
				$mSku .= "-";
			}
			$mSku .= $mRef;
		}

		$mageProd = $this->getProductToFormat($product, $mSku);

		$isNew = $mageProd->getId() ? false : true;
		if (!$isNew && !$overwriteWebsite) {
			$importToWebsiteArray = $mageProd->getWebsiteIds();
		}

		$overwriteProductImages = $helper->getSettings('overwrite_product_image') ? true: false;
		$defaultImportedStatus = $helper->getSettings('default_imported_status');
		if ($discontinued) 
		{
			$defaultImportedStatus = \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED;
			$mageProd->setStatus($defaultImportedStatus);
		}
		$defaultImportedVisibility = $helper->getSettings('default_imported_visibility');

		if ($isNew) {
			$mageProd->addData(array(
				'status' => $defaultImportedStatus,
				'visibility' => $defaultImportedVisibility,
			));
		}

		$mageProd->setName($this->loadSerialisedData('name'))
				->setSku($mSku)
				->setTypeId('simple');
		$mageProd->setWebsiteIds($importToWebsiteArray);
		if ($isNew) {
			$mageProd->setUrlKey($this->createUrlKey($storeRepository, $this->loadSerialisedData('name'), $mSku));
		}

		//get stores app config mapping
		$appConfigMapping = $this->getAppConfigMapping($storeRepository);

		//Company ref id
		$companyRefs = explode("\n", $helper->getCompanyRefIds());
		$supplierName = preg_replace('/[^a-z0-9]/', '', strtolower($this->loadSerialisedData('supplier')));
		$matchedId = null;

		$updatedCompanyRefs = array();
		foreach($companyRefs as $companyRef)
		{
			@list($id, $name) = explode(',', $companyRef);
			$key = preg_replace('/[^a-z0-9]/', '', @strtolower($name));
			$updatedCompanyRefs[$key] = $companyRef;
			if($supplierName == $key)
			{
				$matchedId = intval(trim($id));
				$secondarySupplierId = '';
				if (strpos($id,'|')!='') {
					$parts = explode('|',$id);
					if ($parts[1]!='') $secondarySupplierId = $parts[1];
				}
				break;
			}
		}

		if(!$matchedId)
		{
			$updatedCompanyRefs[$supplierName] = ', '.$this->loadSerialisedData('supplier');
			//$this->getDatasource()->saveConfig('company_ref_ids', implode("\n", $updatedCompanyRefs));
			$this->logger->info('Could not import SKU '.$mSku.' because supplier "'.$this->loadSerialisedData('supplier').'" has no ID set');
			return null;
		}

		/*
		if ($groupingByPrintSizes) {
			$printSizes = $this->getSerialisedData('print_sizes');
		}
		*/


		//Prices
		$pricesData = $this->extractPrices($this->loadSerialisedData('tiers'),$matchedId);


		$mageProd->setPrice($pricesData->mainPrice)
				->setMinSaleQty($pricesData->mainQuantity)
				->setUseConfigMinSaleQty($pricesData->mainQuantity ? false : true);


		$isNew = $mageProd->getId() ? false : true;
		//Set up all remaining attributes

		$g3dattribute = '';
		foreach($this->ATTR_MAP as $src => $target)
		{
			$serialisedData = $this->loadSerialisedData($src);
			if ($target == 'g3d_app_url_default')
			{
				if (substr($serialisedData,0,4)=='http')
				{
					$serialisedData = str_replace('http:','',$serialisedData);
					$serialisedData = str_replace('https:','',$serialisedData);
				}
				$iframeUrl = $serialisedData;
			}
			if ($target == 'g3d_supplier') $supplierCode = $serialisedData;

			if ($target == 'g3d_app_url_default')
			{
				$url = $serialisedData.'&guid='.$matchedId;
				if ($isNew)
				{
					$mageProd->setData($target, $url);
				}
				else
				{
					if ($helper->getSettings("overwrite_default_iframe_urls")) {
						$mageProd->setData($target, $url);
					}
				}
			}
			else if ($target == 'description')
			{
				if ($isNew)
				{
					$mageProd->setData($target, nl2br($serialisedData));
				}
				else
				{
					if ($helper->getSettings("overwrite_product_description")) {
						$mageProd->setData($target, nl2br($serialisedData));
					}
				}
			}
			else if ($target == 'short_description')
			{
				if ($isNew)
				{
					$mageProd->setData($target, nl2br($serialisedData));
				}
				else
				{
					if ($helper->getSettings("overwrite_product_short_description")) {
						$mageProd->setData($target, nl2br($serialisedData));
					}
				}
			}
			else if ($target == "g3d_external_image")
			{
				$externalImage = $serialisedData;

				if ($external3dImage != null)
				{
					$externalImage = $external3dImage;
				}

				$mageProd->setData($target, $externalImage);
			}
			else if ($target == "attribute_sets" && $helper->getSettings('attribute_from_cpp_attribute_sets')) {
				foreach ($serialisedData as $item) {
					$g3dattribute .= '<h2>'.$item['name'].'</h2>';
					$g3dattribute .= '<div>'.$item['description'].'</div>';
				}
				$mageProd->setData('g3d_attribute', $g3dattribute);
			}
			else if ($target == 'weight') {
				if (is_numeric($serialisedData) && $serialisedData != 0) {
					$mageProd->setData($target, $serialisedData / $weightDivider);
				}
			}
			else
			{
				$mageProd->setData($target, $serialisedData);
			}
		}


		$ecommerce = $this->loadSerialisedData('ecommerce');

		if ($ecommerce) {
			foreach($this->ATTR_MAP_ECOMMERCE as $src => $target)
			{
				$ecommerceData = $ecommerce[0][$src];

				if ($target=='g3d_dimensions')
				{
					$ecommerceData = implode(' x ', explode('x', $ecommerceData));
				}

				if ($target=='g3d_packaging_sku')
				{
					$packagingSku = $ecommerceData;
				}

				if ($target=='meta_title')
				{
					$metaTitle = $ecommerceData;
					if ($overwriteMetaTitle) {
						$mageProd->setData($target, $ecommerceData );
					}
				}
				elseif ($target=='meta_description')
				{
					if ($overwriteMetaDescription)
					{
						$mageProd->setData($target, $ecommerceData );
					}
				}
				else
				{
					$mageProd->setData($target, $ecommerceData );
				}
			}
		}

		//add cpp epa to iframe url
		if ($helper->getSettings("cpp_epa"))
		{
			$epaUrl = $helper->getSettings('epa_url');	
			$add = $epaUrl ? $epaUrl : 'https://legacy.custom-gateway.net/acp/api/p/2/epa/get/p/';
			$threedurl = $this->loadSerialisedData('personaliseit_iframe_url').'&epa='.$add;
			if ($matchedId) 
			{
				$threedurl .= '&guid='.$matchedId;
			}
			
			if ($isNew)
			{
				$mageProd->setData('g3d_app_url_default', $threedurl);
			}
			else
			{
				if ($helper->getSettings("overwrite_default_iframe_urls")) {
					$mageProd->setData('g3d_app_url_default', $threedurl);
				}
			}
		}

		//Categories
		$oldCategories = [];
		if (!$isNew)
		{
			$oldCategories = $mageProd->getCategoryIds();
		}
		$this->applyCategories($categoryFactory->create(), $mageProd, $this->loadSerialisedData('category_refs'), $oldCategories);

		//tax class
		$mageProd->setTaxClassId($this->mapTax($helper));

		if (!$isNew)
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$connection = $objectManager->create('\Magento\Framework\App\ResourceConnection');
	        $conn = $connection->getConnection();
	        $sql = "DELETE FROM catalog_product_entity_tier_price WHERE entity_id = ".$mageProd->getId()." AND customer_group_id != 2";
	        $conn->query($sql);
	    }

		$tierPrices = [];
		//Tier Prices
		foreach ($pricesData->tiers as $tier) {
			if ($tier['quantity'] != 1) {

				$tierPrices[] = array(
		            'website_id'  => 0,
		            'cust_group'  => 32000,
		            'price_qty'   => $tier['quantity'],
		            'price'       => $tier['price']
	    		);

				$mageProd->setTierPrice($tierPrices);
			}
		}

		if (isset($pricesData->mainTrade)) {
			$importTradePriceToWholesaleGroup = $helper->getSettings('import_trade_price_to_wholesale_group') ? true: false;
			if ($importTradePriceToWholesaleGroup) {
				$tierPrices[] = array(
					'website_id'  => 0,
					'cust_group'  => 2,
					'price_qty'   => 1,
					'price'       => $pricesData->mainTrade
				);
				$mageProd->setTierPrice($tierPrices);
			}
		}


		//update prices for subsites
		if ($overwriteWebsite)
		{
			$importWebsite = $importToWebsiteArray;
		}
		else
		{
			if (!$isNew)
			{
				$importWebsite = $mageProd->getWebsiteIds();
			}
			else
			{
				$importWebsite = $importToWebsiteArray;
			}
		}

		$mageProd->save();

		$stores = $storeRepository->getList();
		foreach ($stores as $store)
		{
			try
			{
				$pricesDataWebsite = $this->extractPrices($this->loadSerialisedData('tiers'), $matchedId, $store['website_id']);
				if ($store['store_id'] != 0)
				{
					$productResourceModel->load($mageProd, $mageProd->getId());
					$mageProd->setStoreId($store['store_id']);
					$mageProd->setPrice($pricesDataWebsite->mainPrice);
					$productResourceModel->saveAttribute($mageProd, 'price');

					if ($isNew) 
					{
						$defaultImportedVisibility = $helper->getSettings('default_imported_visibility', $store['website_id']);
						$defaultImportedStatus = $helper->getSettings('default_imported_status', $store['website_id']);
						$mageProd->setVisibility($defaultImportedVisibility);
						$mageProd->setStatus($defaultImportedStatus);
						$productResourceModel->saveAttribute($mageProd, 'visibility');
						$productResourceModel->saveAttribute($mageProd, 'status');
					}

					if ($discontinued) 
					{
						$defaultImportedStatus = \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED;
						$mageProd->setStatus($defaultImportedStatus);
						$productResourceModel->saveAttribute($mageProd, 'status');
					}

					$overwriteMetaTitle = $helper->getSettings('overwrite_product_meta_title', $store['website_id']);
					if ($overwriteMetaTitle)
					{
						$mageProd->setMetaTitle($metaTitle);
						$productResourceModel->saveAttribute($mageProd, 'meta_title');
					}

					if (array_key_exists($store['website_id'], $appConfigMapping))
					{	
						$mappedUrl = str_replace($appConfigMapping[$store['website_id']]['from'], $appConfigMapping[$store['website_id']]['to'], $url);
						$mageProd->setData('g3d_app_url_default', $mappedUrl);
						$productResourceModel->saveAttribute($mageProd, 'g3d_app_url_default');
					}

					if ($isNew && $moduleManager->isEnabled('Gateway3D_ProductLayout')) 
					{
						$mageProd->setData('g3d_product_layout', '3');
						$productResourceModel->saveAttribute($mageProd, 'g3d_product_layout');
					}

				}

			}
			catch (Exception $e)
			{
			   $this->logger->info('G3D: error while setting price to product '.$mageProd->getId().' '.$e->getMessage());
			}

		}


		$dateNow = new \DateTime('NOW');

		$this
			->setProductId( $mageProd->getId() )
			->setProcessed(1)
			->setUpdatedAt($dateNow->format('y-m-d H:i:s'))
			->save();

		if ($ecommerce) {
		//1. if has parentSku, check if parentSku product is in magento.
		//2. if yes, assign this product to parentSku product as associated
		//3. if no, create grouped parentSku product in magento, and then do 2.
		$parentSku = $ecommerce[0]['parent_sku'];
		$parentName = $ecommerce[0]['parent_name'];

			if (($parentSku!='') && ($parentName!=''))
			{
				$groupedId = $product->getIdBySku($parentSku);
				if ($groupedId)
				{
					//assign simple to grouped
				}
				else
				{
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
					$productFactory = $objectManager->get('\Magento\Catalog\Model\ProductFactory');

					$grouped = $productFactory->create();
					$attributeSetId = $mageProd->getDefaultAttributeSetId();
					$grouped->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID);
					//$product->setWebsiteIds([$this->storeManager->getDefaultStoreView()->getWebsiteId()]);
					$grouped->setTypeId('grouped');
					$grouped->addData(array(
						'name' => $parentName,
						'sku' => $parentSku,
						'attribute_set_id' => $attributeSetId,
						//'status' => $defaultImportedStatus,
						'status' => 1,
						'visibility' => $defaultImportedVisibility,
						'weight' => 1,
						'tax_class_id' => 0,
						'description' => $mageProd->getDescription(),
						'short_description' => $mageProd->getShortDescription()
					));
					$grouped->setData('g3d_external_image', $externalImage );

					$productLink = $objectManager->create('Magento\Catalog\Api\Data\ProductLinkInterface');
					$productLink->setSku($grouped->getSku())
						->setLinkType('associated')
						->setLinkedProductSku($mageProd->getSku())
						->setLinkedProductType($mageProd->getTypeId())
						->setPosition(1)
						->getExtensionAttributes()
						->setQty(1);

					$associated[] = $productLink;
					$grouped->setProductLinks($associated);
					$grouped->setWebsiteIds($importToWebsiteArray);
					$this->applyCategories($categoryFactory->create(), $grouped, $this->loadSerialisedData('category_refs'), $oldCategories);
					$grouped->setStatus($defaultImportedStatus);
					$grouped->save();


					foreach ($stores as $store) {
						try
						{
							$productResourceModel->load($grouped, $grouped->getId());
							$grouped->setStoreId($store['store_id']);
							$defaultImportedStatus = $helper->getSettings('default_imported_status', $store['website_id']);
							$defaultImportedVisibility = $helper->getSettings('default_imported_visibility', $store['website_id']);
							if ($discontinued) 
							{
								$defaultImportedStatus = \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED;
							}

							if ($isNew && $moduleManager->isEnabled('Gateway3D_ProductLayout')) {
								$grouped->setData('g3d_product_layout', '3');
								$productResourceModel->saveAttribute($grouped, 'g3d_product_layout');
							}

							$grouped->setStatus($defaultImportedStatus);
							$grouped->setVisibility($defaultImportedVisibility);
							$grouped->save();
						}
						catch (Exception $e)
						{
						   $this->logger->info('G3D: error while setting status of product '.$grouped->getId().' '.$e->getMessage());
						}
					}
				}
			}
		}
	}

	private function getAppConfigMapping($storeRepository)
	{	
		$appConfigMapping = [];
		$stores = $storeRepository->getList();
		
		foreach ($stores as $store)
		{
			try
			{	
				if ($store['store_id'] != 0)
				{	
					$mapping = explode(PHP_EOL, $this->_helper->getSettings('app_configuration_mapping', $store['store_id']));
					foreach ($mapping as $mappingLine) {
						if (trim($mappingLine) != '')
						{
							$line = explode("|", $mappingLine);
							if (count($line)) 
							{
								$appConfigMapping[$store['website_id']]['from'][] = trim($line[0]);
								$appConfigMapping[$store['website_id']]['to'][] = trim($line[1]);
							}
						}
					}
					
				}
			}
			catch (Exception $e)
			{
			   $this->logger->info('G3D: error while getting app config mapping '.$mageProd->getId().' '.$e->getMessage());
			}
		}
		return $appConfigMapping;
	}

	private function mapTax($helper) 
	{
		$default = $helper->getDefaultTaxClass();
		for ($i = 1; $i <= 3; ++$i) 
		{
			$code = $this->_helper->getSettings('tax_class_' . $i . '_code');
			if ($code == '') 
			{
				continue;
			}

			$mapping = $this->_helper->getSettings('tax_class_' . $i . '_mapping');
			if ($this->loadSerialisedData('tax_code') == $code)
			{
				return $mapping;
			}
			else
			{
				return $default;
			}
		}
		return null;
	}

	private function extractPrices(array $priceSet, $matchedId, $websiteId = 0)
	{
	 	$useRRP = ($this->_helper->getSettings('price_source', $websiteId) ==
			\Gateway3D\AutoImport\Model\Config\G3dprice::CONFIG_RRP);

		$priceData = new \Magento\Framework\DataObject();
		$priceData->mainPrice = 0;
		$priceData->mainQuantity = 0;
		$priceData->tiers = array();
		$priceData->mainRrp = 0;

		if(count($priceSet) <= 0) return $priceData;

		$quantities = array();
		$prices = array();
		$newPriceSet = array();
		$nullPriceSet = array();
		foreach($priceSet as &$price)
		{
			if (($price['company_ref_id']==$matchedId) && ($price['quantity']!=1))
			{
				$newPriceSet[] = $price;
			}

			if ($price['company_ref_id']==null)
			{
				$nullPriceSet[] = $price;
			}
		}

		if (count($newPriceSet)==0)
		{
			$newPriceSet[] = $nullPriceSet[0];
		}

		foreach($newPriceSet as &$price)
		{
			if(isset($price['quantity']))
			{
				$quantities[] = $price['quantity'];
			}
			else
			{
				$quantities[] = 0;
			}

			if(isset($price['price']) === false)
			{
				$price['price'] = 0;
			}
		}

		array_multisort($quantities, SORT_ASC, $newPriceSet);

		// apply price rules
		$newPrices = array(); // create array in the format applyPriceRules needs
		$tradePrices = array();
		foreach($newPriceSet as &$price)
		{
			$newPrices[$price['quantity']] = $useRRP ? $price['rrp'] : $price['price'];
			$tradePrices[$price['quantity']] = $price['price'];
		}


		//$newPrices = $this->applyPriceRules($newPrices);
		foreach($newPriceSet as &$price)
		{ // update our prices with the new prices
			$price['price'] = $newPrices[$price['quantity']];
			$price['trade'] = $tradePrices[$price['quantity']];
		}

		$priceData->mainPrice = $newPriceSet[0]['price'];
		$priceData->mainQuantity = $newPriceSet[0]['quantity'];
		$priceData->mainRrp = $newPriceSet[0]['rrp'];
		$priceData->mainTrade = $newPriceSet[0]['trade'];

		//unset($newPriceSet[0]);
		if(count($newPriceSet) > 0)
		{
			$priceData->tiers = $newPriceSet;
		}

		return $priceData;
	}

	public function applyCategories($categoryFactory, $mageProd, $categories, $oldCategories, $parents = true )
	{
		if( !is_array($categories))
		{
			$categories = array( $categories );
		}

		$newCats = array();
		foreach( $categories as $catCode )
		{
			if( $catId = $this->getMatchingMageCategoryId( $catCode, $categoryFactory ) )
			{
				$newCats[] = $catId;
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$mageCat = $objectManager->create('Magento\Catalog\Model\Category')->load($catId);

				if( $parents and $mageCat and $path = $mageCat->getPath() )
				{
					$parents = explode( '/', $path );
					array_shift( $parents ); // remove the root
					foreach( $parents as $parent )
					{
						$newCats[] = $parent;
					}
				}
			}
		}



		// keep existing non-datasource categories on the product:

		$existingCats = $mageProd->getCategoryIds();

		if( is_array( $existingCats ) )
		{
			foreach( $existingCats as $existingCat )
			{
				if( ! $this->getMatchingDatasourceCategoryCode( $existingCat ) )
				{
					$newCats[] = $existingCat;
				}
			}
		}
		$newCats = array_unique( $newCats );

		$newCats = array_merge($newCats, $oldCategories);

		if( is_array( $existingCats ) )
		{
			sort( $existingCats );
		}
		else
		{
			$existingCats = [];
		}
		sort( $newCats );

		$changed = ( implode( ',', $existingCats ) != implode( ',', $newCats ) );
		$mageProd->setCategoryIds( $newCats );
		return $changed;

	}

	public function getMatchingMageCategoryId( $datasourceCode, $categoryFactory )
	{
		$collection = $categoryFactory->getCollection()
					->addFieldToFilter('datasource_name', 'g3d')
					->addFieldToFilter('datasource_code', $datasourceCode);

		if( $collection->getSize() == 0 )
		{
			return null;
		}
		$item = $collection->getFirstItem();
		if( $item->getCategoryId() == 0 )
		{
			return null;
		}

		return $item->getCategoryId();
	}


	public function getProductToFormat($product, $mSku)
	{
		$mageProd = $this->getMageProduct($product, $mSku);
		if(!$mageProd)
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$productFactory = $objectManager->get('\Magento\Catalog\Model\ProductFactory');

			$mageProd = $productFactory->create();
			$attributeSetId = $mageProd->getDefaultAttributeSetId();
			$mageProd->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID);
			$mageProd->setTypeId('simple');
			$mageProd->addData(array(
				'name' => $this->loadSerialisedData("name"),
				'attribute_set_id' => $attributeSetId,
				'weight' => 1,
				'tax_class_id' => 0,
				'description' => '',
				'short_description' => ''

			));
		}
		return $mageProd;
	}


	public function getMageProduct($product, $mSku)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $pro = $objectManager->get('Magento\Catalog\Model\Product');
		$id = $pro->getIdBySku($mSku);

		if ($id != 0) {
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$productFactory = $objectManager->get('\Magento\Catalog\Model\ProductFactory');
			return $productFactory->create()->load($id);
		}
		
		if( $id == 0 and $mSku != '' )
		{
			$this->setProductId( $id );
			return null;
			
		}
	}


	public function loadSerialisedData( $item = null )
	{

		if( $this->serialisedData === null )
		{
			$ser = $this->getSerialised();
			$serData = array();
			if( !empty( $ser ) )
			{
				$serData = unserialize( $ser );
			}
			$this->serialisedData = $serData;
		}
		if( $item === null )
		{
			return $this->serialisedData;
		}
		if( !isset( $this->serialisedData[ $item ] ) )
		{
			return null;
		}
		return $this->serialisedData[ $item ];
	}

	public function setSerialisedData( $item, $value = null )
	{
		if( is_array( $item ) )
		{
			foreach( $item as $k=>$v )
			{
				$this->serialisedData[ $k ] = $v;
			}
		}
		else
		{
			$this->serialisedData[ $item ] = $value;
		}

		return $this->serialisedData;
	}
}
