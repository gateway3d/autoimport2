<?php

namespace Gateway3D\AutoImport\Model;

use Magento\Framework\Model\AbstractModel;

class Category extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{	

	const CACHE_TAG = 'gateway3d_autoimport_category';

	protected $_cacheTag = 'gateway3d_autoimport_category';

	protected $_eventPrefix = 'gateway3d_autoimport_category';

	protected $serialisedData = null;
	protected $logger;
	protected $_applyParentToExistingCategory = true;
	
	/**
	 * Define resource model
	 */
	protected function _construct()
	{	
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/autoimport.log');
		$myLogger = new \Zend\Log\Logger();
		$myLogger->addWriter($writer);
		$this->logger = $myLogger;

		$this->_init('Gateway3D\AutoImport\Model\ResourceModel\Category');
	}


	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];
		return $values;
	}

	public function updateMagento($categoryFactory) 
	{
		$mageCat=$this->getCategoryToFormat();
		$hasChanged = false;
		$fieldsToCheck = array();

		if( $this->_applyParentToExistingCategory or ! $mageCat->getId() )
		{
		
			if( !$this->applyParent( $mageCat, $categoryFactory ) )
			{
				// parent was not found, so leave this category for the next run:
				return null;
			}
			$fieldsToCheck[] = 'parent_id';
			
		}

		/*
		$img = $this->getSerialisedData('image');
		$this->applyImage( $mageCat, $img ); // also handles blanks
		$fieldsToCheck[] = 'image';
		*/

		if( $this->getDeleted() )
		{
			$mageCat->setIsActive( 0 );
		}
		else
		{
			$mageCat->setIsActive( 1 );
		}
		$fieldsToCheck[] = 'is_active';

		if($mageCat->getId() == NULL)
		{
			$this->logger->info("CHANGING CAT NAME:".$mageCat->getId()."; STORE--ID=".$mageCat->getStoreId()."::resource=".$mageCat->getResourceName());
			$mageCat->setName( $this->loadSerialisedData('name') );

			$siteUrl = strtolower($this->loadSerialisedData('name'));
			$cleanUrl = trim(preg_replace('/ +/', '', preg_replace('/[^A-Za-z0-9 ]/', '', urldecode(html_entity_decode(strip_tags($siteUrl))))));
			$mageCat->setUrlKey($cleanUrl);
			$fieldsToCheck[] = 'name';
		}

		foreach( $fieldsToCheck as $field )
		{
			if( ( $mageCat->getData( $field ) != $mageCat->getOrigData( $field ) ) and !is_array( $mageCat->getData( $field ) ) )
			{
				$hasChanged = true;
				//Mage::log( '--- category changed '.$field.' was "'.$mageCat->getOrigData( $field ).'" now "'.$mageCat->getData( $field ).'"' );
			}
		}

		if( ! $mageCat->getId() or $hasChanged )
		{
			$this->logger->info( 'category "'.$mageCat->getName().'" has changed, saving...' );
			$mageCat->save();
		}

		$this
			->setCategoryId( $mageCat->getId() )
			->setProcessed(1)
			->save();

		$this->logger->info('....done, ID='.$mageCat->getId());
		return $mageCat;
	}


	public function applyParent( $mageCat, $categoryFactory )
	{

		if( $this->getParentCode() == '' )
		{
			// top level category, put it under mage cat ID 1:
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$store = 1;
			$mageStore = $storeManager->getStore($store);
			$rootNodeId = $mageStore->getRootCategoryId();
			$parent = $objectManager->create('Magento\Catalog\Model\Category')->load($rootNodeId);
		}
		else 
		{
			$parent = $this->getMatchingMageCategory( $this->getParentCode(), $categoryFactory );
		}

		if( !$parent or !$parent->getId() )
		{
			$this->logger->info("Skipping ".$this->getDatasourceName()." category ".$this->getDatasourceCode().", parent ".$this->getParentCode().' is not available yet');
			return false;
		}
		$parentId = $parent->getId();
		if($mageCat->getId()) // existing category, so parent and path are already set
		{
			$this->logger->info('...existing category '.$mageCat->getId().', moving if required');
			//if($mageCat->getParentId() != $parentId) // if parent has changed, move it
			//{
			//	$mageCat->move($parentId,null);
			//}
		}
		else
		{
			$this->logger->info('...new category, setting parent path: '.$parent->getPath());
			$mageCat->setPath($parent->getPath());
		}
		return true;
	}


	public function getMatchingMageCategoryId( $datasourceCode, $categoryFactory )
	{
		$collection = $categoryFactory->getCollection()
					->addFieldToFilter('datasource_name', 'g3d')
					->addFieldToFilter('datasource_code', $datasourceCode);
					

		if( $collection->getSize() == 0 )
		{
			return null;
		}
		$item = $collection->getFirstItem();
		if( $item->getCategoryId() == 0 )
		{
			return null;
		}

		return $item->getCategoryId();
	}

	public function getMatchingMageCategory( $datasourceCode, $categoryFactory )
	{
		$id = $this->getMatchingMageCategoryId( $datasourceCode, $categoryFactory );
		if( !$id )
		{
			return null;
		}

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		return $objectManager->create('Magento\Catalog\Model\Category')->load($id);
	}



	public function getCategoryToFormat() {
		$mageCat = $this->getMageCategory();
		if( !$mageCat )
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			// catagory name
			$categoryFactory=$objectManager->get('\Magento\Catalog\Model\CategoryFactory');
			
			$mageCat = $categoryFactory->create();
			$mageCat->setIsActive(true);
		}
		return $mageCat;
	}

	public function getMageCategory()
	{
		if( $this->getCategoryId() == 0 )
		{
			return null;
		}

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		return $objectManager->create('Magento\Catalog\Model\Category')->load($this->getCategoryId());
	}

	public function loadSerialisedData( $item = null )
	{	

		if( $this->serialisedData === null )
		{
			$ser = $this->getSerialised();
			$serData = array();
			if( !empty( $ser ) )
			{
				$serData = unserialize( $ser );
			}
			$this->serialisedData = $serData;
		}
		if( $item === null )
		{
			return $this->serialisedData;
		}
		if( !isset( $this->serialisedData[ $item ] ) )
		{
			return null;
		}
		return $this->serialisedData[ $item ];
	}

	public function setSerialisedData( $item, $value = null ) {
		//$this->loadSerialisedData();
		if( is_array( $item ) )
		{
			foreach( $item as $k=>$v )
			{
				$this->serialisedData[ $k ] = $v;
			}
		}
		else 
		{
			$this->serialisedData[ $item ] = $value;
		}

		return $this->serialisedData;
	}

}