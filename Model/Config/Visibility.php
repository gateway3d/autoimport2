<?php
namespace Gateway3D\AutoImport\Model\Config;
 
class Visibility implements \Magento\Framework\Option\ArrayInterface
{
	/**
	 * @return array
	 */
	public function toOptionArray()
	{
		$options = array(
			array('value' => \Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE, 'label' => __('Nowhere')),

			array('value' => \Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_CATALOG, 'label' => __('Catalog')),

			array('value' => \Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_SEARCH, 'label' => __('Search')),

			array('value' => \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH, 'label' => __('Catalog, Search')),

		);

		return $options;
	}
}