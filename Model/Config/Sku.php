<?php
namespace Gateway3D\AutoImport\Model\Config;
 
class Sku implements \Magento\Framework\Option\ArrayInterface
{
	/**
	 * @return array
	 */
	public function toOptionArray()
	{
		return [
			['value' => 'sku', 'label' => __('Supplier SKU')],
			['value' => 'retail_sku', 'label' => __('Retail SKU')]
		];
	}
}