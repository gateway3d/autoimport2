<?php
namespace Gateway3D\AutoImport\Model\Config;

class G3dprice implements \Magento\Framework\Option\ArrayInterface
{
    const CONFIG_PRICE = 0;
    const CONFIG_RRP = 1;

    public function toOptionArray()
    {
        return array(
            array('value' => self::CONFIG_PRICE, 'label' => 'price'),
            array('value' => self::CONFIG_RRP, 'label' => 'RRP')
        );
    }

}
