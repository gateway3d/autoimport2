<?php
namespace Gateway3D\AutoImport\Model\Config;

class Status implements \Magento\Framework\Option\ArrayInterface
{
	/**
	 * @return array
	 */
	public function toOptionArray()
	{
		$options = array(
			array('value' => \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED, 'label' => __('Enabled')),

			array('value' => \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED, 'label' => __('Disabled'))
		);

		return $options;
	}
}
