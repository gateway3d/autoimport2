<?php
namespace Gateway3D\AutoImport\Model\Config;

class TaxClass implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {   
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $taxClassObj = $objectManager->create('Magento\Tax\Model\TaxClass\Source\Product');
        $taxClasses = $taxClassObj->getAllOptions();
        
        return $taxClasses;
    }
}
