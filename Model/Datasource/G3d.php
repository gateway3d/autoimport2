<?php
namespace Gateway3D\AutoImport\Model\Datasource;

use Magento\Framework\Model\AbstractModel;
use Gateway3D\AutoImport\Helper\Data;
use Gateway3D\AutoImport\Model\Category;
use Gateway3D\AutoImport\Model\Product;
use Magento\Framework\App\Action\Context;
use Magento\Store\Model\StoreRepository;

class G3d extends AbstractModel  
{
	
	protected $_modelCategory;
	
	const mainParentCategory = 'DIRECTORY'; //Directory, must be set up before useing the code

	protected $_displayName = 'Gateway3D CPP';
	protected $_httpClient = false;
	protected $_httpUrlAddon = '/auto-import/1/';
	protected $_httpAuthUrlAddon = '/p/1/auth/';
	protected $_httpAuthUrl;
	protected $_userName;
	protected $_userPass;
	protected $_userId;
	protected $_categories;   
	protected $productUpdateOverride = 1440; 
	protected $productRequiredFields;

	protected $_helper;
	protected $_categoryFactory;
	protected $_productFactory;
	protected $_storeRepository;
	protected $_productResourceModel;
	protected $_magentoProductFactory;
	protected $_moduleManager;
	protected $logger;

	protected $serialisedData = null;
	protected $_productsImported = array();
	protected $_categoriesImported = array();
	protected $_importedSkus = array();
	protected $_updateMagentoProductsPageSize;
	protected $addDatasourceCodeToSku;
	protected $importSkuFrom;
	

	public function __construct(
	   \Gateway3D\AutoImport\Helper\Data $helper,
	   \Gateway3D\AutoImport\Model\CategoryFactory $categoryFactory,
	   \Gateway3D\AutoImport\Model\ProductFactory $productFactory,
	   \Magento\Framework\App\ResourceConnection $resource,
	   \Magento\Store\Model\StoreRepository $storeRepository,
	   \Magento\Catalog\Model\ResourceModel\Product $productResourceModel,
	   \Magento\Framework\Module\Manager $moduleManager
	) 
	{
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/autoimport.log');
		$myLogger = new \Zend\Log\Logger();
		$myLogger->addWriter($writer);
		$this->logger = $myLogger;
		$this->_helper = $helper;
		$this->_categoryFactory = $categoryFactory;
		$this->_productFactory = $productFactory;
		$this->_storeRepository = $storeRepository;
		$this->_productResourceModel = $productResourceModel;
		$this->_moduleManager = $moduleManager;
		$this->_resource = $resource;
		$this->_httpUrl = $helper->getApiUrl().$this->_httpUrlAddon;
		$this->_httpAuthUrl = $helper->getApiUrl().$this->_httpAuthUrlAddon;
		$this->_userName = $helper->getCppUsername();
		$this->_userPass = $helper->getCppPassword();
		$this->_updateMagentoProductsPageSize = 100;      
		$this->productRequiredFields = array("ref",
			"name",
			"sku",
			"category_refs");   
		$this->addDatasourceCodeToSku = $helper->getSettings('add_datasource_code_to_sku') ? true: false;
		$this->importSkuFrom = $helper->getSettings('import_sku_from');
		$this->productUpdateOverride = $helper->getGeneral('product_update_override');
	}

	public function cronjob() 
	{	
		$this->importCategories();
		$this->updateMagentoCategories();
		$this->importProducts();
		$this->updateNewMagentoProducts();
		$this->updateMagentoProducts();
	}

	public function getDatasourceItem($id) 
	{
		$category = $this->_categoryFactory->create();
		$collection = $category->getCollection()
						->addFieldToFilter('datasource_code', $id);
		if ($collection->count() == 0) 
		{
			$date = new \DateTime('NOW');
			$data = [
				'datasource_code' => $id,
				'datasource_name' => 'g3d',
				'processed' => 0,
				'updated_at' => $date->format('y-m-d H:i:s')           
			];
			$category->setData($data);
			$category->save();
		}

		$updatedCategory = $this->_categoryFactory->create();
		return $updatedCategory->getCollection()->addFieldToFilter('datasource_code', $id)->getFirstItem();
	}

	public function importCategories() 
	{
		$date = new \DateTime('NOW');
		$date->sub(new \DateInterval('PT'.$this->productUpdateOverride.'M'));
		
		$category = $this->_categoryFactory->create();
		$agedCategories = $category->getCollection()
						->addFieldToFilter('updated_at', array('lt' => $date->format('y-m-d H:i:s')))
						->addFieldToFilter('processed', 1)
						->addFieldToFilter('deleted', 0);
		$anyCategory = $category->getCollection()
						->addFieldToFilter('deleted', 0);

		if($agedCategories->getSize() <= 0 && $anyCategory->getSize() > 0)
		{
			return false;
		}

		$this->setMainParentCategory();

		$json = $this->doHttpRequest('categories', array('user_id' => $this->_userId ));
		$categories = json_decode($json, true);
		$this->_categories = array();
		if (isset($categories['categories']) and is_array($categories['categories'])) 
		{
			$this->_categories = $categories['categories'];
		} 
		else 
		{
			$this->logger->info('Failed to get categories from G3D system');
		}

		$catCount = 0;
		foreach ($this->_categories as $cat) 
		{
			$dsCat = $this->getDatasourceItem($cat['id']);

			$changed = (	
				$dsCat->loadSerialisedData('name') 	!= $cat['name'] 		||
				$dsCat->loadSerialisedData('image') 	!= $cat['image_url']	||
				$dsCat->getParentCode()				!= $this->getBestMatchingCategoryParent($cat['parent_id'], $this->_categories)	||
				$dsCat->getDeleted()				== 1
			);

			//if ($changed && $dsCat->getName != self::mainParentCategory) {
			if ($changed && $dsCat->getDatasourceCode() != self::mainParentCategory) 
			{
				$matchingParentID = $this->getBestMatchingCategoryParent($cat['parent_id'], $this->_categories);
				
				$ser = $dsCat->setSerialisedData(
							array('name' => $cat['name'],
								'image' => $cat['image_url']
							));	

				$dateNow = new \DateTime('NOW');

				$dsCat->setSerialised(serialize($ser))
						->setParentCode($matchingParentID)
						->setUpdatedAt($dateNow->format('y-m-d H:i:s'))
						->setDeleted(0)
						->setProcessed(0)
						->save();

				$catCount++;	
			}
			$this->_categoriesImported[] = $cat['id'];
		}
		if(count($this->_categoriesImported) > 0){
			//$this->markDeletedCategories();
		}
		$this->logger->info("G3D import categories::Updated $catCount categories");			
	}

	private function updateMagentoCategories() 
	{
		$category = $this->_categoryFactory->create();
		$collection = $category->getCollection();

		if( $collection->getSize() == 0 )
		{	
			return false; // nothing to do
		}

		$this->logger->info('doing updateMagentoCategories for '.$collection->count().' categories');

		foreach( $collection as $cat )
		{
			$cat->updateMagento($category);
		}
		return true;
	}
	
	private function getBestMatchingCategoryParent($parentCandidateID, $arrivingCategoriesSet) 
	{
		foreach ($arrivingCategoriesSet as $cat) 
		{
			if ($cat['id'] == $parentCandidateID) 
			{
				return $parentCandidateID;
			}
		}
		return self::mainParentCategory;
	}


	protected function setMainParentCategory() 
	{
		$category = $this->_categoryFactory->create();
		
		if ($category->getCollection()->getSize() == 0) 
		{ // DIRECTORY top level does not exist
			$date = new \DateTime('NOW');
			$data = [
				'datasource_code' => self::mainParentCategory,
				'datasource_name' => 'g3d',
				'processed' => 0,
				'updated_at' => $date->format('y-m-d H:i:s')           
			];
			$category->setData($data);
			$category->save();
		}

		$firstItem = $category->getCollection()->getFirstItem();
		$firstItem->setProcessed(0);
		$ser = $firstItem->setSerialisedData(array('name' => self::mainParentCategory));
		$firstItem->setSerialised(serialize($ser));
		$firstItem->save();
		
		return false;
	}

	public function getShortName() 
	{
		return 'g3d';
	}

	public function importProducts()
	{
		//is there product not older than 30mins ?
		$date = new \DateTime('NOW');
		$date->sub(new \DateInterval('PT30M'));
		$products = $this->_productFactory->create();
		$newProducts = $products->getCollection()
				->addFieldToFilter('imported_at', array('lt' => $date->format('y-m-d H:i:s')));
		$allProducts = $products->getCollection();


		$date2 = new \DateTime('NOW');
		$date2->sub(new \DateInterval('PT2M'));
		$recentlyImportedProducts = $products->getCollection()
				->addFieldToFilter('imported_at', array('gt' => $date->format('y-m-d H:i:s')));

		if ($newProducts->getSize() > 0 || $allProducts->getSize() == 0) {	
			$this->importRecentlyAddedProducts();
		} 
	}



	protected function importRecentlyAddedProducts() 
	{
		$params = array();
		return $this->processProductRequest('products/get/', $params);
	}


	protected function processProductRequest($receiver, $params = array()) 
	{
		$this->logger->info('G3d import products::Start');
		$pCount = 0;
		$nextPage = $this->getSetNextPage();
		//$nextPage = 0;
		//$currentPage = 0;
		$lastPage = 0;
		$iteration = 0;
		do 
		{
			$params['page'] = $nextPage;           
			$response = json_decode($this->doHttpRequest($receiver, $params), true);

			if (isset($response['pages'])) 
			{
				if(isset($response['pages']['last'])) $lastPage = $response['pages']['last'];
				
				if(isset($response['pages']['next'])) $nextPage = $response['pages']['next'];
				//	else $nextPage = 0;
				
				if(isset($response['pages']['current'])) $currentPage = $response['pages']['current'];
				
				$this->getSetNextPage($nextPage);
			}
			

			if (isset($response["products"]) == false) 
			{
				$this->logger->info('G3d products import::No proper response. Breaking');
				$this->logger->info(serialize($response));

				return false;
			}
			
			$pSaveCount = $this->saveProductPage($response["products"]);
			$pCount += $pSaveCount;
			
			if($pSaveCount == 0)
			{
				$this->getSetNextPage('0');
				$nextPage = 0;
			}

			//building log message
			$products = $response["products"];

			$logContents = "G3d products import::";
			$logContents .= "current page: $currentPage";
			$logContents .= "; next page: $nextPage";
			$logContents .= "; products count: " . count($products);

			reset($products);
			$product = current($products);
			$logContents .= "; first: " . $product["ref"];

			end($products);
			$product = current($products);
			$logContents .= "; last: " . $product["ref"];
			$this->logger->info($logContents);
			$iteration++;
		} 
		while($nextPage);
		
		$this->logger->info('G3d products import::Finished, imported $pCount products in $iteration iterations.');
		
		if($currentPage == $iteration)
		{
			return true;
		}
		return false;
	}


	public function getDatasourceProduct($id) 
	{
		$product = $this->_productFactory->create();
		$collection = $product->getCollection()
						->addFieldToFilter('datasource_code', $id);
		
		if ($collection->count() == 0) 
		{
			$date = new \DateTime('NOW');
			$data = [
				'datasource_code' => $id,
				'datasource_name' => 'g3d',
				'processed' => 0,
				'updated_at' => $date->format('y-m-d H:i:s')           
			];
			$product->setData($data);
			//$product->save();
			return $product;
		}

		$updatedProduct = $this->_productFactory->create();
		return $updatedProduct->getCollection()->addFieldToFilter('datasource_code', $id)->getFirstItem();
	}


	protected function saveProductPage($productsPage) 
	{	
		$pCount = 0;
		foreach ($productsPage as $product) 
		{
			
			if ($this->productValid($product) == false) 
			{
				$this->logger->info("G3d product import::Could not save product::id:" . $product["id"] . "; sku:" . $product["sku"] . "; ref:" . $product["ref"]);
				continue;
			}
			
			
			$dsProd = $this->getDatasourceProduct($product['sku'] . "-" . $product["ref"]);
			$checkSku = $product[$this->importSkuFrom];
			if ($this->addDatasourceCodeToSku) 
			{
				$checkSku .= '-' . $product['ref'];
			}


			if(in_array($dsProd->getDatasourceCode(), $this->_productsImported)){
				//$this->logger->info("Duplicated:".$dsProd->getDatasourceCode());
				continue;
			}

			if ($this->checkIfUnique($checkSku)) 
			{	
				$dateNow = new \DateTime('NOW');
				$ser = $dsProd->setSerialisedData($product);
				$dsProd ->setSerialised(serialize($ser))
						->setDeleted(0)
						->setProcessed(0)
						->setStatus('')
						->setImportedAt($dateNow->format('y-m-d H:i:s'))
						->setCppLastUpdate($ser['last_modified'])
						->save();
				$this->_productsImported[$dsProd->getDatasourceCode()] = $dsProd->getDatasourceCode();
				$this->_importedSkus[] = $checkSku;
				$pCount++;
			}
		}
		return $pCount;
	}

	private function checkIfUnique($sku) 
	{
		if (in_array($sku,$this->_importedSkus)) 
		{
			return false;
		} 
		else 
		{	
			return true;
		}
	}



	public function updateNewMagentoProducts() 
	{	
		$product = $this->_productFactory->create();
		$collection = $product->getCollection()
			->addFieldToFilter('datasource_name', $this->getShortName() )
			->addFieldToFilter('processed', 0 )
			->addFieldToFilter('product_id', array('null' => true))
			->addFieldToFilter('parent_code', '' )
			->setPageSize($this->_updateMagentoProductsPageSize);

		if( $collection->getSize() == 0 )
		{
			$this->logger->info($this->getShortName());
			return false; // nothing to do
		}
		
		$this->logger->info('updating Magento prods for '.$collection->count().' '.$this->getShortName().' products');
		$counter = 0;
		foreach( $collection as $prod ) 
		{
			$prod->updateMagento(
				$product, 
				$this->_helper, 
				$this->_categoryFactory, 
				$this->_productFactory,
				$this->_storeRepository,
				$this->_productResourceModel,
				$this->_moduleManager
			);
		}
	}

	public function updateMagentoProducts() 
	{	
		$date = new \DateTime('NOW');
		$date->sub(new \DateInterval('PT'.$this->productUpdateOverride.'M'));

		$product = $this->_productFactory->create();
		$collection = $product->getCollection()
			->addFieldToFilter('datasource_name', $this->getShortName() )
			->addFieldToFilter('parent_code', '' )
			->addFieldToFilter('cpp_last_update', array('gt' => $date->format('y-m-d H:i:s')))   //NEW
			->addFieldToFilter('updated_at', array('lt' => $date->format('y-m-d H:i:s')))   //NEW
			->setPageSize($this->_updateMagentoProductsPageSize);

		if( $collection->getSize() == 0 )
		{	
			$this->logger->info('NEWLOG: nothing to update');
			return false; // nothing to do
		}
		
		$this->logger->info('updating Magento prods for '.$collection->count().' '.$this->getShortName().' products');
		$counter = 0;
		foreach( $collection as $prod ) 
		{
			$prod->updateMagento(
				$product, 
				$this->_helper, 
				$this->_categoryFactory, 
				$this->_productFactory,
				$this->_storeRepository,
				$this->_productResourceModel,
				$this->_moduleManager
			);
		}
	}

	protected function markDeletedProducts(){
		$products = $this->_productFactory->create();
		$toDelete = $products->getCollection()
						->addFieldToFilter('deleted', 0)
						->addFieldToFilter('datasource_code', array('nin'=>$this->_productsImported));

		$this->logger->info("Products to delete:.". $toDelete->count());						
		$delCount = 0;
		foreach($toDelete as $product){
			$product->setDeleted(1)
					->save()
			;
			$delCount++;
		}		
		$this->logger->info("Deleted $delCount products");
	}

	protected function doHttpRequest($request, $params = array()) 
	{
		if (!$this->_httpClient) 
		{
			// Establish connection
			$this->getHttpConnection();
		}
		
		$attempts = 3;

		while ($attempts--) 
		{
			$this->logger->info("trying to get categories...");		
			$this->_httpClient->setUri($this->_httpUrl . $request);
			$this->_httpClient->setMethod('GET');
			$this->_httpClient->setParameterGet($params);
			$response = $this->_httpClient->send();		
			
			if ($response->isServerError() && $attempts != 0) 
			{
				$this->logger->info('G3D: Error with connection - error ' . $response->getStatusCode());
				// If it's failed here, try reconnecting/authenticating and loop around if appropriate.
				$this->getHttpConnection();
			}
			else 
			{
				// Success. Return response.
				return $response->getBody();
			}
		}
	}

	protected function getHttpConnection() 
	{
		// Create/reauthenticate a HTTP connection
		if (!$this->_httpClient) 
		{
			$this->logger->info('G3D: Creating initial HTTP adaptor');
			$this->_httpClient = new \Zend\Http\Client();
			//$this->_httpClient->setCookieJar();
		}

		// Authenticate
		$this->logger->info('G3D: Authenticating with server, URL: ' . $this->_httpAuthUrl);

		$data = array(
			'username' => $this->_userName, 
			'password' => $this->_userPass
		);

		$this->_httpClient->setUri($this->_httpAuthUrl);
		$this->_httpClient->setMethod('POST');
		$this->_httpClient->setRawBody(json_encode($data));
		$response = $this->_httpClient->send();	
		$this->_userId = json_decode($response->getBody())->user_id;

		if ($response->isServerError()) 
		{
			$this->logger->info('G3D: An error occurred authenticating to the G3D source. The server replied with '.$response->getStatusCode());
		}
		$this->logger->info('G3D: User ID: ' . $this->_userId);
	}

	private function productValid($productSet) 
	{
		if (is_array($productSet) == false) 
		{
			return false;
		}
		foreach ($this->productRequiredFields as $field) 
		{
			if (array_key_exists($field, $productSet) == false && $productSet[$field] === NULL) 
			{
				return false;
			}
		}
		return true;
	}


	private function getSetNextPage($value = NULL) 
	{
		if ($value !== NULL) 
		{
			file_put_contents(BP . "/var/log/G3dPage", $value);
			return NULL;
		}

		if (file_exists(BP . "/var/log/G3dPage")) 
		{
			return file_get_contents(BP . "/var/log/G3dPage");
		}
		return 0;
	}
}
