# Gateway3d AutoImport2 #

### Getting the Code via SSH & Composer ###
make sure that you have ``"minimum-stability": "dev"` in your main magento2 composer file.
update "require" and "repositories" parts in main magento2 composer file

```
"require": {
    "gateway3d/autoimport": "@dev"
}


"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:gateway3d/autoimport2.git"
    }
]

```

Then run following command

```
$ composer update
```

you should see something like this

```
Loading composer repositories with package information
Updating dependencies (including require-dev)             
  - Installing gateway3d/autoimport (dev-master e51a8bf)
    Cloning e51a8bf961ae49e5eb92cbc6c1e0e6cf9eed81db
Writing lock file
Generating autoload files
```

# Updating the Database #
Subsequently, the Magento command line tool must now also be used to update the database schema.Then you would be probably asked to recompile. After that set permissions and clean cache.
```
php bin/magento setup:upgrade
php bin/magento setup:di:compile
sudo chmod 777 var -R && sudo chmod 777 pub -R
 n98-magerun2.phar cache:clean && n98-magerun2.phar cache:flush && sudo chmod 777 var -R && sudo chmod 777 pub -R
```

# Cron and Permissions #
Please make sure that cron is running correctly for your site and also you have set correct permissions for your site. There is autoimport log under var/log/autoimport.log.


# Local Testing #
Model/DataSource/G3d.php - getHttpConnection
```
$options = array(
    'sslallowselfsigned' => true,
    'sslverifypeer' => false,
    'sslverifypeername' => false
);

$this->_httpClient = new \Zend\Http\Client(null, $options);
```
