<?php

namespace Gateway3D\AutoImport\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use Gateway3D\AutoImport\Model\Datasource;
use Gateway3D\AutoImport\Model\Category;
use Gateway3D\AutoImport\Model\Product;


class Data extends AbstractHelper
{
	public function getDatasourceModel($categoryFactory, $productFactory, $resource, $storeRepository, $productResourceModel, $moduleManager) 
	{
		//$categoryModel = new \Gateway3D\AutoImport\Model\Category();
		return new \Gateway3D\AutoImport\Model\Datasource\G3d(
			$this,
			$categoryFactory, 
			$productFactory, 
			$resource,
			$storeRepository,
			$productResourceModel,
			$moduleManager
		);
	}

	//general
	public function getScheduledImportEnabled() 
	{
	 	return $this->scopeConfig->getValue('autoimport/general/g3dautoimport_g3d_active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

	public function getApiUrl() 
	{
	 	return $this->scopeConfig->getValue('autoimport/general/g3dautoimport_g3d_g3d_api_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

	public function getCppUsername() 
	{
	 	return $this->scopeConfig->getValue('autoimport/general/g3dautoimport_g3d_g3d_username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
	}

	public function getCppPassword() 
	{
		return $this->scopeConfig->getValue('autoimport/general/g3dautoimport_g3d_g3d_password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);   
	}

	public function getCompanyRefIds() 
	{
		return $this->scopeConfig->getValue('autoimport/general/g3dautoimport_g3d_company_ref_ids', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);   
	}

	
	//settings
	public function getSettings($key, $storeCode = null) 
	{
		return $this->scopeConfig->getValue('autoimport/settings/g3dautoimport_settings_'.$key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeCode);   
	}

	public function getGeneral($key, $storeCode = null) 
	{
		return $this->scopeConfig->getValue('autoimport/general/g3dautoimport_g3d_'.$key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeCode);   
	}
	
	public function getWeightUnit($storeCode = null) 
	{
		return $this->scopeConfig->getValue('general/locale/weight_unit', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeCode);   
	}

	public function getDefaultTaxClass($storeCode = null) 
	{
		return $this->scopeConfig->getValue('tax/classes/default_product_tax_class', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeCode);   
	}
}