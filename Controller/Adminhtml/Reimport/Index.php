<?php
namespace Gateway3D\AutoImport\Controller\Adminhtml\ReImport;
use Gateway3D\AutoImport\Model\Product;


class Index extends \Magento\Backend\App\Action
{
	protected $resultPageFactory;

	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Gateway3D\AutoImport\Model\ProductFactory $productFactory,
		\Magento\Framework\Message\ManagerInterface $messageManager
		) 
	{
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
		$this->productFactory = $productFactory;
		$this->messageManager = $messageManager;
	}

	public function execute()
	{
		
		$id = $this->getRequest()->getParam('id');
		$product = $this->productFactory->create();
		$collection = $product->getCollection()
			->addFieldToFilter('product_id', $id );

		if ($collection->getSize() == 0) {
			$message = 'This product is not being tracked by the AutoImport module so cannot be re-imported.';
		} 
		else 
		{   
			$message = 'The re-import option for the Gateway3D CPP data source will refresh ALL products at the next opportunity.';
			$first = $collection->getFirstItem();
			$first->setStatus('reimport');
			$first->save();
		}
			
		$this->messageManager->addWarning($message);

		$url =  $this->getUrl('catalog/product/edit', ['id' => $id ]);
		$this->_redirect($url); 
	}

 
}