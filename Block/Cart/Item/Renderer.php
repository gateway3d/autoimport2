<?php

namespace Gateway3D\AutoImport\Block\Cart\Item;

use Magento\Checkout\Block\Cart\Item\Renderer\Actions;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Framework\View\Element\Message\InterpretationStrategyInterface;
use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\Catalog\Pricing\Price\ConfiguredPriceInterface;

class Renderer
	extends \Magento\Checkout\Block\Cart\Item\Renderer
{
	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Product\Configuration $productConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\Module\Manager $moduleManager,
        InterpretationStrategyInterface $messageInterpretationStrategy,
        array $data = []
    ) {
		parent::__construct($context, $productConfig, $checkoutSession, $imageBuilder, $urlHelper, $messageManager, $priceCurrency, $moduleManager, $messageInterpretationStrategy, $data);
	}

    public function getOptionList()
	{
		return array_filter(parent::getOptionList(), function($option)
		{
			return $option['label'] != 'Personalisation Ref';
		});
	}

	public function getImage($product, $imageId, $attributes = [])
	{
	
		$image = parent::getImage($product, $imageId, $attributes);

		$buyRequest = json_decode($this->getItem()->getOptionByCode('info_buyRequest')->getValue(), true);

		if(isset($buyRequest['g3d'][0]['thumbnails']))
		{
			$matches = array_filter($buyRequest['g3d'][0]['thumbnails'], function($thumbnail)
			{
				return $thumbnail['name'] == 'thumbnail_default';
			});

			if(count($matches))
			{
				$default = reset($matches);
			}
			else
			{
				if(count($buyRequest['g3d'][0]['thumbnails'])) 
				{
					$default = $buyRequest['g3d'][0]['thumbnails'][0];
				}
				else 
				{
					$default['url'] = '';
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
					$productFactory = $objectManager->get('\Magento\Catalog\Model\ProductFactory');
					$pro = $productFactory->create()->load($product->getId());
					$externalImage = $pro->getData('g3d_external_image');
					$default['url'] = $externalImage;
				}
			}

			if($default['url'] != '') 
			{
				$image->setImageUrl($default['url']);
			}
		}
		
		return $image;
	}

	protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Checkout\Block\Cart\Item\Renderer'));
        return parent::_toHtml();
    }
}

