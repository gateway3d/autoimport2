<?php
namespace Gateway3D\AutoImport\Block\Product\View;

class Gallery extends \Magento\Catalog\Block\Product\View\Gallery
{
	/**
	* Retrieve product images in JSON format
	*
	* @return string
	*/
	public function getGalleryImagesJson()
	{
		$imagesItems = [];
	
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$product = $objectManager->get('Magento\Framework\Registry')->registry('current_product');
		$url = $product->getData('g3d_external_image');
			
		if ($url != '') {
			$imagesItems[] = [
				'thumb' => $url,
				'img' => $url,
				'full' => $url,
				'caption' => '',
				'position' => '0',
				'isMain' => true,
				'type' => 'image',
				'videoUrl' => null,
			];
		}

		if (count($this->getGalleryImages())>0) {
			foreach ($this->getGalleryImages() as $image) {
				$imagesItems[] = [
					'thumb' => $image->getData('small_image_url'),
					'img' => $image->getData('medium_image_url'),
					'full' => $image->getData('large_image_url'),
					'caption' => ($image->getLabel() ?: $this->getProduct()->getName()),
					'position' => $image->getPosition(),
					'isMain' => $this->isMainImage($image),
					'type' => str_replace('external-', '', $image->getMediaType()),
					'videoUrl' => $image->getVideoUrl(),
				];
			}
		}

		if ($url == '' && count($this->getGalleryImages()) == 0) {
			$imagesItems[] = [
				'thumb' => $this->_imageHelper->getDefaultPlaceholderUrl('thumbnail'),
				'img' => $this->_imageHelper->getDefaultPlaceholderUrl('image'),
				'full' => $this->_imageHelper->getDefaultPlaceholderUrl('image'),
				'caption' => '',
				'position' => '0',
				'isMain' => true,
				'type' => 'image',
				'videoUrl' => null,
			];
		}

		return json_encode($imagesItems);
	}
}
