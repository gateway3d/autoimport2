<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Gateway3D\AutoImport\Block\Product;

use Magento\Catalog\Helper\ImageFactory as HelperFactory;
use Magento\Catalog\Model\Product;

/**
 * @deprecated 103.0.0
 * @see ImageFactory
 */
class ImageBuilder extends \Magento\Catalog\Block\Product\ImageBuilder
{

    /**
     * Create image block
     *
     * @param Product|null $product
     * @param string|null $imageId
     * @param array|null $attributes
     * @return Image
     */
    public function create(Product $product = null, string $imageId = null, array $attributes = null)
    {
        $product = $product ?? $this->product;
        $imageId = $imageId ?? $this->imageId;
        $attributes = $attributes ?? $this->attributes;
        return $this->imageFactory->create($product, $imageId, $attributes);
    }
}
