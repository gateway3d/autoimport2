<?php 
namespace Gateway3D\AutoImport\Block\Adminhtml\Product\Edit\Button;

use Magento\Catalog\Block\Adminhtml\Product\Edit\Button\Generic;

class ReImport extends Generic
{
	public function getButtonData()
	{	
		$url =  $this->getUrl('autoimport/reimport/index', ['id' => $this->getProduct()->getId() ]);
		return [
			'label' => __('Re-Import'),
			'url' => $url,
			'sort_order' => 100,
			'class' => 'action-secondary'
		];
	}
}