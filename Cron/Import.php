<?php
namespace Gateway3D\AutoImport\Cron;

use Gateway3D\AutoImport\Helper\Data;

class Import 
{
 
	protected $_logger;
	protected $_helper;
	protected $_categoryFactory;
	protected $_productFactory;
	protected $_resource;
	protected $_storeRepository;
	protected $_moduleManager;
	
	public function __construct(
		\Psr\Log\LoggerInterface $logger,
		\Gateway3D\AutoImport\Helper\Data $helper,
	 	\Gateway3D\AutoImport\Model\CategoryFactory $categoryFactory,
		\Gateway3D\AutoImport\Model\ProductFactory $productFactory,
	 	\Magento\Framework\App\ResourceConnection $resource,
		\Magento\Store\Model\StoreRepository $storeRepository,
		\Magento\Catalog\Model\ResourceModel\Product $productResourceModel,
		\Magento\Framework\Module\Manager $moduleManager
	) 
	{
		$this->_logger = $logger;
		$this->_helper = $helper;
		$this->_categoryFactory = $categoryFactory;
		$this->_productFactory = $productFactory;
		$this->_resource = $resource;
		$this->_storeRepository = $storeRepository;
		$this->_productResourceModel = $productResourceModel;
		$this->_moduleManager = $moduleManager;
	}
	
	/**
	 * Method executed when cron runs in server
	 */
	public function execute() 
	{
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/autoimport.log');
		$myLogger = new \Zend\Log\Logger();
		$myLogger->addWriter($writer);
		if (!$this->_helper->getScheduledImportEnabled()) 
		{
			$myLogger->info("G3D: autoimport is disabled");
			return false;
		}
		$ds = $this->_helper->getDatasourceModel(
			$this->_categoryFactory,
			$this->_productFactory,
			$this->_resource,
			$this->_storeRepository,
			$this->_productResourceModel,
			$this->_moduleManager
		);
		$ds->cronjob();
		
		return $this;
	}
}