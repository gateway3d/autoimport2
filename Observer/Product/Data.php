<?php
namespace Gateway3D\AutoImport\Observer\Product;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Gateway3D\AutoImport\Model\Product;

class Data implements ObserverInterface
{   
	protected $messageManager;
	protected $productFactory;
	
	public function __construct(
		\Magento\Framework\Message\ManagerInterface $messageManager,
		\Gateway3D\AutoImport\Model\ProductFactory $productFactory
	)
	{
		$this->messageManager = $messageManager;
		$this->productFactory = $productFactory;
	}
	/**
	 * Below is the method that will fire whenever the event runs!
	 *
	 * @param Observer $observer
	 */
	public function execute(Observer $observer)
	{
		$productId = $observer->getProduct()->getId();
		$products = $this->productFactory->create();

		$dsProd = $products->getCollection()
						->addFieldToFilter('product_id' , $productId)
						->getFirstItem();
		if($dsProd && $dsProd->getId())
		{
			$msg = '';
			if ($dsProd->getCppLastUpdate() != '0000-00-00 00:00:00') 
			{
				$msg .= 'Last update in CPP ' . $dsProd->getCppLastUpdate().'.';
			}

			if ($dsProd->getUpdatedAt() != '0000-00-00 00:00:00') 
			{
				$msg .= 'Last update in Magento  ' . $dsProd->getUpdatedAt().'.';
			}

			if ($msg)
			{
				$this->messageManager->addSuccess($msg);
			}
		}
	}
}