<?php

namespace Gateway3D\AutoImport\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();

		$tableName = $installer->getTable('gateway3d_autoimport_datasource_abstract_category');
		// Check if the table already exists
		if ($installer->getConnection()->isTableExists($tableName) != true) 
		{
			$table = $installer->getConnection()
				->newTable($tableName)
				->addColumn(
					'id',
					Table::TYPE_INTEGER,
					10,
					[
						'identity' => true,
						'unsigned' => true,
						'nullable' => false,
						'primary' => true
					],
					'ID'
				)
				->addColumn(
					'datasource_code',
					Table::TYPE_TEXT,
					255,
					['nullable' => false, 'default' => ''],
					'Datasource code'
				)
				->addColumn(
					'datasource_name',
					Table::TYPE_TEXT,
					255,
					['nullable' => false, 'default' => ''],
					'Datasource name'
				)
				->addColumn(
					'updated_at',
					Table::TYPE_DATETIME,
					null,
					['nullable' => false],
					'Updated At'
				)
				->addColumn(
					'parent_code',
					Table::TYPE_TEXT,
					255,
					['nullable' => false, 'default' => ''],
					'Parent code'
				)
				->addColumn(
					'category_id',
					Table::TYPE_INTEGER,
					10,
					['unsigned' =>  true],
					'Category Id'
				)
				->addColumn(
					'processed',
					Table::TYPE_INTEGER,
					4,
					['nullable' => false, 'default' => 0],
					'Processed'
				)    
				->addColumn(
					'status',
					Table::TYPE_TEXT,
					255,
					['nullable' => false, 'default' => ''],
					'Status'
				)
				->addColumn(
					'serialised',
					Table::TYPE_BLOB,
					null,
					['nullable' => false, 'default' => ''],
					'Serialised'
				)
				->addColumn(
					'deleted',
					Table::TYPE_INTEGER,
					4,
					['nullable' => false, 'default' => 0],
					'Deleted'
				)
				->addForeignKey(
					$installer->getFkName('gateway3d_autoimport_datasource_abstract_category', 'category_id', 'catalog_category_entity', 'entity_id'),
					'category_id',
					$installer->getTable('catalog_category_entity'),
					'entity_id',
				\Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
				)
				->setComment('Gateway3D Autoimport Datasource Abstract Category')
				->setOption('type', 'InnoDB')
				->setOption('charset', 'utf8');
			$installer->getConnection()->createTable($table);
		}


		$tableName = $installer->getTable('gateway3d_autoimport_datasource_abstract_product');
		// Check if the table already exists
		if ($installer->getConnection()->isTableExists($tableName) != true) 
		{
			$table = $installer->getConnection()
				->newTable($tableName)
				->addColumn(
					'id',
					Table::TYPE_INTEGER,
					10,
					[
						'identity' => true,
						'unsigned' => true,
						'nullable' => false,
						'primary' => true
					],
					'ID'
				)
				->addColumn(
					'datasource_code',
					Table::TYPE_TEXT,
					255,
					['nullable' => false, 'default' => ''],
					'Datasource code'
				)
				->addColumn(
					'datasource_name',
					Table::TYPE_TEXT,
					255,
					['nullable' => false, 'default' => ''],
					'Datasource name'
				)
				->addColumn(
					'updated_at',
					Table::TYPE_DATETIME,
					null,
					['nullable' => false],
					'Updated At'
				)
				->addColumn(
					'parent_code',
					Table::TYPE_TEXT,
					255,
					['nullable' => false, 'default' => ''],
					'Parent code'
				)
				->addColumn(
					'product_id',
					Table::TYPE_INTEGER,
					10,
					['unsigned' =>  true],
					'Product Id'
				)
				->addColumn(
					'processed',
					Table::TYPE_INTEGER,
					4,
					['nullable' => false, 'default' => 0],
					'Processed'
				)    
				->addColumn(
					'status',
					Table::TYPE_TEXT,
					255,
					['nullable' => false, 'default' => ''],
					'Status'
				)
				->addColumn(
					'serialised',
					Table::TYPE_BLOB,
					null,
					['nullable' => false, 'default' => ''],
					'Serialised'
				)
				->addColumn(
					'deleted',
					Table::TYPE_INTEGER,
					4,
					['nullable' => false, 'default' => 0],
					'Deleted'
				)
				->addForeignKey(
					$installer->getFkName('gateway3d_autoimport_datasource_abstract_product', 'product_id', 'catalog_product_entity', 'entity_id'),
					'product_id',
					$installer->getTable('catalog_product_entity'),
					'entity_id',
				\Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
				)
				->setComment('Gateway3D Autoimport Datasource Abstract Product')
				->setOption('type', 'InnoDB')
				->setOption('charset', 'utf8');
			$installer->getConnection()->createTable($table);
		}

		$installer->endSetup();
	}
}