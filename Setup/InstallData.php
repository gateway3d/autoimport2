<?php

namespace Gateway3D\AutoImport\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
	/**
	 * EAV setup factory
	 *
	 * @var EavSetupFactory
	 */
	private $eavSetupFactory;
	private $storeRepository;

	/**
	 * Init
	 *
	 * @param EavSetupFactory $eavSetupFactory
	 */
	public function __construct(
		EavSetupFactory $eavSetupFactory,
		\Magento\Store\Model\StoreRepository $storeRepository
	)
	{
		$this->eavSetupFactory = $eavSetupFactory;
		$this->storeRepository = $storeRepository;
	}

	/**
	 * {@inheritdoc}
	 */
	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		/** @var EavSetup $eavSetup */
		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);


		/*
		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_datasource_code');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_supplier');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_type');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_lead_time');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_price_includes');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_manufacturers_code');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_selling_unit');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_third_party_url');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_barcode');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_rrp');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_notes');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_website_category_details');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_brand_code');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_design_name');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_licence');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_lead_time_days');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_standard_margin');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_tax_code');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_increment_quantity');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_certificate_url');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_production_instructions');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_brand_name');

		$eavSetup->removeAttribute(
          \Magento\Catalog\Model\Product::ENTITY,
            'g3d_brand_name_short');
		*/


		$eavSetup
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_external_image',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'External image',
					'required' => false,
					'sort_order' => 5,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_datasource_code',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Datasource Code',
					'required' => false,
					'sort_order' => 10,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_supplier',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Supplier',
					'required' => false,
					'sort_order' => 20,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_type',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Type',
					'required' => false,
					'sort_order' => 30,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_lead_time',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Lead Time',
					'required' => false,
					'sort_order' => 40,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_price_includes',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Price Includes',
					'required' => false,
					'sort_order' => 50,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_manufacturers_code',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Manufacturers Code',
					'required' => false,
					'sort_order' => 60,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_selling_unit',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Selling Unit',
					'required' => false,
					'sort_order' => 70,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_third_party_url',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Third Party Personalisation URL',
					'required' => false,
					'sort_order' => 80,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_barcode',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Barcode',
					'required' => false,
					'sort_order' => 90,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_rrp',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Rrp',
					'required' => false,
					'sort_order' => 100,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_notes',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'textarea',
					'label' => 'Notes',
					'required' => false,
					'sort_order' => 110,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_website_category_details',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Website Category Details',
					'required' => false,
					'sort_order' => 120,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_brand_code',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Brand Code',
					'required' => false,
					'sort_order' => 130,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_design_name',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Design Name',
					'required' => false,
					'sort_order' => 140,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_licence',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Licence',
					'required' => false,
					'sort_order' => 150,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_lead_time_days',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Lead Time Days',
					'required' => false,
					'sort_order' => 160,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_standard_margin',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Standard Margin',
					'required' => false,
					'sort_order' => 170,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_tax_code',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Tax Code',
					'required' => false,
					'sort_order' => 180,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_increment_quantity',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Increment Quantity',
					'required' => false,
					'sort_order' => 190,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_certificate_url',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Certificate Url',
					'required' => false,
					'sort_order' => 200,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_production_instructions',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Production Instructions',
					'required' => false,
					'sort_order' => 210,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_brand_name',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Brand Name',
					'required' => false,
					'sort_order' => 220,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			)
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_brand_name_short',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D AutoImport',
					'input' => 'text',
					'label' => 'Brand Name (Short)',
					'required' => false,
					'sort_order' => 230,
					'used_in_product_listing' => true,
                	'backend' => '',
                	'source' => '',
                	'visible_on_front' => false,
                	'option' => [
                    	'values' => [],
               		]
                ]
			);

			$websitesIds = '';
			$stores = $this->storeRepository->getList();
			foreach ($stores as $store)
			{
				if ($store['website_id'] != 0)
				{
					$websitesIds .= $store['website_id'] . ',';
				}
			}
			$websitesIds = substr($websitesIds, 0, -1);

            $data = [
                'scope' => 'default',
                'scope_id' => 0,
                'path' => 'autoimport/settings/g3dautoimport_settings_import_to_website',
                'value' => $websitesIds
            ];
            $setup->getConnection()
                ->insertOnDuplicate($setup->getTable('core_config_data'), $data, ['value']);

	}
}
