<?php

namespace Gateway3D\AutoImport\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements  UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup,
                            ModuleContextInterface $context){
        $setup->startSetup();
        if (version_compare($context->getVersion(), '2.3.2.3') < 0) {

            // Get module table
            $tableName = $setup->getTable('gateway3d_autoimport_datasource_abstract_product');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'imported_at' => [
                        'type' => Table::TYPE_DATETIME,
                        'nullable' => false,
                        'comment' => 'Imported at',
                    ],
                ];

                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }

        if (version_compare($context->getVersion(), '2.3.2.8') < 0) {

            // Get module table
            $tableName = $setup->getTable('gateway3d_autoimport_datasource_abstract_product');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'cpp_last_update' => [
                        'type' => Table::TYPE_DATETIME,
                        'nullable' => false,
                        'comment' => 'CPP Last update',
                    ],
                ];

                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }

        $setup->endSetup();
    }
}