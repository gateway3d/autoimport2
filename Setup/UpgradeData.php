<?php

namespace Gateway3D\AutoImport\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
	/**
	 * EAV setup factory
	 *
	 * @var EavSetupFactory
	 */
	private $eavSetupFactory;
	private $storeRepository;

	/**
	 * Init
	 *
	 * @param EavSetupFactory $eavSetupFactory
	 */
	public function __construct(
		EavSetupFactory $eavSetupFactory,
		\Magento\Store\Model\StoreRepository $storeRepository
	)
	{
		$this->eavSetupFactory = $eavSetupFactory;
		$this->storeRepository = $storeRepository;
	}

	/**
	 * {@inheritdoc}
	 */
	public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{

		if (version_compare($context->getVersion(), '1.0.0.4') < 0) {
			/** @var EavSetup $eavSetup */
			$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);


			$eavSetup
				->addAttribute(
					\Magento\Catalog\Model\Product::ENTITY,
					'g3d_attribute',
					[
						'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
						'group' => 'Attributes',
						'input' => 'textarea',
						'label' => 'G3d Attribute Set',
						'required' => false,
						'sort_order' => 5,
						'used_in_product_listing' => true,
						'backend' => '',
						'type' => 'text',
						'source' => '',
						'visible_on_front' => true,
						'wysiwyg_enabled' => true,
						'option' => [
							'values' => [],
						]
					]
				);
		}
		
		if (version_compare($context->getVersion(), '2.3.2.4') < 0) {
			/** @var EavSetup $eavSetup */
			$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);				

			$groupId = (int)$eavSetup->getAttributeGroupByCode(
                \Magento\Catalog\Model\Product::ENTITY,
                'Default',
                'gateway3d-autoimport',
                'attribute_group_id'
            );
			
			$eavSetup->updateAttributeGroup(
				\Magento\Catalog\Model\Product::ENTITY,
				'Default', 
				$groupId,  
				'attribute_group_name', 
				'Custom Gateway AutoImport'
			);
		}

		if (version_compare($context->getVersion(), '2.3.3.7') < 0) {
			/** @var EavSetup $eavSetup */
			$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

			$eavSetup
				->addAttribute(
					\Magento\Catalog\Model\Product::ENTITY,
					'g3d_hs_code',
					[
						'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
						'group' => 'Gateway3D AutoImport',
						'input' => 'text',
						'label' => 'HS Code',
						'required' => false,
						'sort_order' => 240,
						'used_in_product_listing' => true,
						'backend' => '',
						'source' => '',
						'visible_on_front' => false,
						'option' => [
							'values' => [],
						]
					]
				);
        }
	}
}
